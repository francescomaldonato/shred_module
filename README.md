## Description
This Shred tool is a program that will overwrite your files in a way that makes them very difficult to recover by a third party.

Normally, when you delete a file, that portion of the disk is marked as being ready for another file to be written to it, but the data is still there. If a third party were to gain physical access to your disk, they could, using advanced techniques, access the data you thought you had deleted.

The way that shred accomplishes this type of destruction digitally is to overwrite (over and over, repeatedly, as many times as you specify) the data you want to destroy, replacing it with other (usually random) data. Doing this magnetically destroys the data on the disk and makes it highly improbable that it can ever be recovered.

### Run the script through Golang
The script can be run normally through the Golang command line:

```
go run main.go
```


## Test cases

- Empty file
- Typical .txt file

## Possible test cases

- Files without permission access
- Files with size larger than the int64 limit
- Irregular files (directories, special-characted named files)

## Authors and acknowledgment
francesco.maldonato97@gmail.com

## License
GNU General Public License (GPL 3.0)