//Package shred provides a function that overwrites a file with random bytes before deleting it
package shred
//package main
 
import (
    "fmt"
	"os"
    "math/rand"
	"time"
)
 
/* func main() {
	
	var file_path string
	file_path = "randomfile.txt"
	Shred(file_path)
	
} */

func Shred(path string) error{
	//Shred function tool that will overwrite the given file (e.g. “randomfile”) 3 times with
	//random data and delete the file afterwards. Note that the file may contain any type of data.

	// Check file length
	fstat, err := os.Stat(path)
	if err != nil {
		fmt.Println(err)
	}
	var fSize int64 = fstat.Size()
	if fSize == 0 {
		err = os.Remove(path)
		if err != nil {
			fmt.Println(err)
		}
	}
	// Open file
	file, err := os.OpenFile(path, os.O_WRONLY, 0777)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	var length int = 16384
    
	// Shredding Loop
	for i:=0; i<3; i++ {
		// Truncating and writing file starting at the beginning
		err = file.Truncate(0)
		_, err = file.Seek(0, 0)
		if err != nil {
			fmt.Println(err)
		}

		//Updating random seed
		rand.Seed(time.Now().UnixNano())
		//length := rand.Intn(16384)
  
		// Getting random bytes
		rand_str := make([]byte, length)
		_, err = rand.Read(rand_str)
		if err != nil {
			fmt.Println(err)
		}
		
		//Write directly into file
		_, err = file.Write(rand_str)
		if err != nil {
			fmt.Println(err)
		}

		//Synch to disk
		err = file.Sync()
		if err != nil {
			fmt.Println(err)
		}
	}

	// Close the file
	file.Close()
	// remove the file
	err = os.Remove(path)
	if err != nil {
		fmt.Println(err)
	}
	return nil
}