package main
 
import (
    "fmt"
	"os"
	"bufio"
	"math/rand"
	"time"
	"shred_module/shred"
)
 
func main() {
	
	var file_path string
	file_path = "emptyFile.txt"
	EmptyFile_test(file_path)

	file_path = "File.txt"
	File_test(file_path)

}

func EmptyFile_test(path string) {

	file, err := os.Create(path)
    if err != nil {
		fmt.Println(err)
	}
	//Synch to disk
	err = file.Sync()
    file.Close()
	//Testing Shred function
	shred.Shred(path)
	fmt.Println("EmptyFile test completed")
}

func File_test(path string) {

	file, err := os.Create(path)
    if err != nil {
		fmt.Println(err)
	}

	//Updating random seed
	rand.Seed(time.Now().UnixNano())
	var length int = 16384

	// Getting random bytes
	rand_str := make([]byte, length)
	_, err = rand.Read(rand_str)
	if err != nil {
		fmt.Println(err)
	}
	
	//Write directly into file
	_, err = file.Write(rand_str)
	if err != nil {
		fmt.Println(err)
	}

	//Synch to disk
	err = file.Sync()
	if err != nil {
		fmt.Println(err)
	}

	// Create a new writer.
    w := bufio.NewWriter(file)
	str_ := string(rand_str[:])
    // Write a string to the file.
    w.WriteString(str_)

    // Flush.
    w.Flush()
    file.Close()
	//Testing Shred function
	shred.Shred(path)
	fmt.Println("File test completed")
}